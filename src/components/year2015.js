import React, { memo, forwardRef } from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import { Container, Section, HeaderYear } from './yearwrap'
import Image from 'gatsby-image'

const Year = memo(({ forwardedRef }) => {  
  const data = useStaticQuery(graphql`
    query {
      ayasanmobile: file(absolutePath: { regex: "/ayasanmobile/" }) {
        childImageSharp {
          fixed(width: 280) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      kaidee: file(absolutePath: { regex: "/kaidee/" }) {
        childImageSharp {
          fixed(width: 300) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      setorth: file(absolutePath: { regex: "/setorth/" }) {
        childImageSharp {
          fixed(width: 140) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `)

  const { kaidee, setorth, ayasanmobile } = data

  return (
    <Container ref={forwardedRef} backgroundColor="#7fa1b5">
      <HeaderYear year="2015"/>
      <Section
        reverse
        title="Ayasan Partnership"
        description={<>He got an invitation to partnered with <a href="https://www.ayasan-service.com">Ayasan Service</a> a Japanese-Thai Maid Agency. He developed <a href="https://play.google.com/store/apps/details?id=com.ayasan.yoda.android">Android</a> and <a href="https://itunes.apple.com/us/app/ayasan/id1025748222">iOS</a> Application to help people who are looking for an on-demand Maid Service. Backend is running with <a href="https://parseplatform.org">Parse</a> and <a href="https://nodejs.org">Nodejs</a></>}
        showcase={
          <Image
            fixed={ayasanmobile.childImageSharp.fixed}
            alt="ayasan app"
          />
        }
      />
      <Section
        title="Acquire Hire by Kaidee.com"
        description={<>Initially he was working at <a href="https://www.kaidee.com" title="kaidee">Kaidee</a> as Android Dev. But React seems to be so awesome that he couldn't resist switch from Android to <a href="https://reactjs.org">React</a> Dev instantly. He also help the team to start a new Python project and maintain a legacy PHP project.</>}
        showcase={
          <Image
            fixed={kaidee.childImageSharp.fixed}
            alt="kaidee"
          />
        }
      />
      <Section
        reverse
        title="Expand to Investment"
        description={<>After settle with Kaidee for sometimes, he expand his knowledge into investment area which become his passion for goods.</>}
        showcase={
          <Image
            fixed={setorth.childImageSharp.fixed}
            alt="set.or.th"
          />
        }
      />
    </Container>
  )
})

export default forwardRef((props, ref) => <Year {...props} forwardedRef={ref} />)
