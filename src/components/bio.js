/**
 * Bio component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */
import React, { memo } from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import { Row, Col, Icon } from 'antd'
import Image from 'gatsby-image'

import { rhythm } from '../utils/typography'

const IconComponent = (props) => <Icon {...props} style={{ fontSize: 18 }}/>

const Bio = () => {
  const data = useStaticQuery(graphql`
    query BioQuery {
      avatar: file(absolutePath: { regex: "/profile-pic.jpg/" }) {
        childImageSharp {
          fixed(width: 60, height: 60) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      site {
        siteMetadata {
          author
          social {
            linkedin
            medium
            github
            facebook
            twitter
            site
          }
        }
      }
    }
  `)
  const { author, social } = data.site.siteMetadata

  return (
    <div
    style={{
      marginLeft: `auto`,
      marginRight: `auto`,
      maxWidth: rhythm(24),
      padding: `0 ${rhythm(3 / 4)}`,
    }}>
      <Row type="flex" style={{ marginBottom: rhythm(1.5) }}>
        <Col>
          <Image
            fixed={data.avatar.childImageSharp.fixed}
            alt={author}
            style={{
              marginRight: rhythm(0.5),
              marginBottom: 0,
              minWidth: 50,
              borderRadius: `100%`,
            }}
            imgStyle={{
              borderRadius: `50%`,
            }}
          />
        </Col>
        <Col xs={19}>
          <Row>
            <p>
              <strong>{author}</strong> is an author of <a href={`https://facebook.com/${social.facebook}`}>
              {social.facebook}
              </a> based in Bangkok. He writes and share knowledge on Investment and related stuff.
              <br/>He is also do <IconComponent type="code" theme="filled"/>ing.
            </p>
          </Row>
          <Row type="flex" gutter={8}>
            <Col>Reach him on -</Col>
            <Col><a href={`https://facebook.com/${social.facebook}`}><IconComponent type="facebook" theme="filled"/></a></Col>
            <Col><a href={`https://linkedin.com/in/${social.linkedin}`}><IconComponent type="linkedin" theme="filled"/></a></Col>
            <Col><a href={`https://medium.com/${social.medium}`}><IconComponent type="medium"/></a></Col>
            <Col><a href={`https://github.com/${social.github}`}><IconComponent type="github" theme="filled"/></a></Col>
            <Col><a href={social.site}><IconComponent type="compass" theme="filled"/></a></Col>
          </Row>
        </Col>
      </Row>
    </div>
  )
}

export default memo(Bio)
