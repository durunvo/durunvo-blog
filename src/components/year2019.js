import React, { memo, forwardRef } from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import { Container, Section, HeaderYear } from './yearwrap'
import Image from 'gatsby-image'

const Year = memo(({ forwardedRef }) => {
  const data = useStaticQuery(graphql`
    query {
      blogpropachill: file(absolutePath: { regex: "/blogpropachill/" }) {
        childImageSharp {
          fixed(width: 300) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      chevron: file(absolutePath: { regex: "/chevron/" }) {
        childImageSharp {
          fixed(width: 80) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      guruport: file(absolutePath: { regex: "/guruport/" }) {
        childImageSharp {
          fixed(width: 300) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `)

  const { chevron, guruport, blogpropachill } = data

  return (
    <Container ref={forwardedRef} backgroundColor="#000b46">
      <HeaderYear year="2019" />
      <Section
        reverse
        title="Launch PropaChill Blog"
        description={<>Later on, he moved his focus to improve PropaChill.com SEO Ranking so he created <a href="https://blog.propachill.com">blog.propachill.com</a> and also do Digital Content Marketing at the same time.</>}
        showcase={
          <Image
            fixed={blogpropachill.childImageSharp.fixed}
            alt="blog propachill"
          />
        }
      />
      <Section
        title="JS Dev. Contractor"
        description={<>In early 2019 joined Chevron Thailand as a JD Dev Contractor. He helps Oil/Gas team build a Drilling Decision Making tools with React and .NetCore and migrate <a href="https://vuejs.org">Vue</a> into React.</>}
        showcase={
          <Image
            fixed={chevron.childImageSharp.fixed}
            alt="chevron"
          />
        }
      />
      <Section
        reverse
        title="Launch GURU Port"
        description={<>At the same time, he is also invited by Finnomena to luanch his first ever <a href="https://www.finnomena.com/investdiary/best-of-risk-adjusted-return/">Best of Risk-Adjusted Return</a> investment strategy portfolio. Suitable for every long-term investor.</>}
        showcase={
          <Image
            fixed={guruport.childImageSharp.fixed}
            alt="guruport"
          />
        }
      />
    </Container>
  )
})

export default forwardRef((props, ref) => <Year {...props} forwardedRef={ref} />)
