import React, { memo, forwardRef } from 'react'
import { Row, Col } from 'antd'
import { Container, Section, Header } from './yearwrap'
import ScrollAnimation from 'react-animate-on-scroll'

const Year = memo(({ forwardedRef }) => {
  return (
    <Container ref={forwardedRef} backgroundColor="#bb3939" noLine>
      <ScrollAnimation animateIn="pulse">
        <Header
          bottom="Thank You"
        />
      </ScrollAnimation>
    </Container>
  )
})

export default forwardRef((props, ref) => <Year {...props} forwardedRef={ref} />)
