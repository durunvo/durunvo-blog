import React, { memo, forwardRef } from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import { Container, Section, HeaderYear } from './yearwrap'
import Image from 'gatsby-image'

const Year = memo(({ forwardedRef }) => {
  const data = useStaticQuery(graphql`
    query {
      invesdiarylogo: file(absolutePath: { regex: "/invesdiarylogo/" }) {
        childImageSharp {
          fixed(width: 80) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      finnomenalogo: file(absolutePath: { regex: "/finnomenalogo/" }) {
        childImageSharp {
          fixed(width: 200) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      vivonalawlogo: file(absolutePath: { regex: "/vivonalawlogo/" }) {
        childImageSharp {
          fixed(width: 250) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `)

  const { invesdiarylogo, vivonalawlogo, finnomenalogo } = data

  return (
    <Container ref={forwardedRef} backgroundColor="#2d1142">
      <HeaderYear year="2017"/>
      <Section
        title="Finnomena's Guest Writer"
        description={<>As Investdiary becoming more popular, he got an invitation to be a guest writer for <a href="https://finnomena.com">Finnomena</a> sharing his knowledge to more audience.</>}
        showcase={
          <Image
            fixed={finnomenalogo.childImageSharp.fixed}
            alt="finnomena"
          />
        }
      />
      <Section
        reverse
        title="Launch Investdiary.co"
        description={<>Blogging at Facebook is not enough. He begins his SEO journey by launch his own website <a href="https://investdiary.co">Investdiary.co</a>. The site is powered by <a href="https://ghost.org">Ghost</a> and hosted on <a href="https://heroku.com">Heroku</a></>}
        showcase={
          <Image
            fixed={invesdiarylogo.childImageSharp.fixed}
            alt="invesdiary"
          />
        }
      />
      <Section
        title="Brand New Vivonalaw.com"
        description={<>He help his friends build a brand new website <a href="https://vivonalaw.com">vivonalaw.com</a> running with Ghost. It improves the SEO performance by more than 100% compare to old site.</>}
        showcase={
          <Image
            fixed={vivonalawlogo.childImageSharp.fixed}
            alt="vivanolaw"
          />
        }
      />
    </Container>
  )
})

export default forwardRef((props, ref) => <Year {...props} forwardedRef={ref} />)
