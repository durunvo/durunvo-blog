import React, { memo, forwardRef } from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import { Container, Section, HeaderYear } from './yearwrap'
import Image from 'gatsby-image'

const Year = memo(({ forwardedRef }) => {
  const data = useStaticQuery(graphql`
    query {
      propachilllogo: file(absolutePath: { regex: "/propachilllogo/" }) {
        childImageSharp {
          fixed(width: 200) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      arcadialogo: file(absolutePath: { regex: "/arcadialogo/" }) {
        childImageSharp {
          fixed(width: 250) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      villapayakam: file(absolutePath: { regex: "/villapayakam/" }) {
        childImageSharp {
          fixed(width: 200) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `)

  const { propachilllogo, arcadialogo, villapayakam } = data

  return (
    <Container ref={forwardedRef} backgroundColor="#5a204b">
      <HeaderYear year="2018" />
      <Section
        reverse
        title="Launch PropaChill.com"
        description={<>He co-founded <a href="https://propachill.com">PropaChill.com</a> as a new property rental platform. The website is built with React/Redux stack and UI with <a href="https://ant.design">Ant.design</a>. He spend most of his time align the product with the team process.</>}
        showcase={
          <Image
            fixed={propachilllogo.childImageSharp.fixed}
            alt="propachill"
          />
        }
      />
      <Section
        title="Join Arcadia Beauty Project"
        description={<>He joined <a href="https://arcadia.in.th">Arcadia Beauty</a> and help the team build its technology stack from scratch. The main site is built with <a href="https://wordpress.org">Wordpress</a> and hosted on <a href="www.alibabacloud.com">Alibaba Cloud</a>.</>}
        showcase={
          <Image
            fixed={arcadialogo.childImageSharp.fixed}
            alt="arcadia beauty"
          />
        }
      />
      <Section
        reverse
        title="Airbnb in Chiang Mai"
        description={<>During this time, he flies to Chiang Mai flequently to helps his family deploy a guest house on the <a href="https://www.airbnb.com/users/show/217218949">Airbnb</a> platform. The guest house named <a href="https://www.facebook.com/villapayakam">"Villa Payakam"</a>.</>}
        showcase={
          <Image
            fixed={villapayakam.childImageSharp.fixed}
            alt="Villa Payakam"
          />
        }
      />
    </Container>
  )
})

export default forwardRef((props, ref) => <Year {...props} forwardedRef={ref} />)
