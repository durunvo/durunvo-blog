import React, { memo, forwardRef } from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import Image from 'gatsby-image'
import { Container, Section, HeaderYear } from './yearwrap'

const Year = memo(({ forwardedRef }) => {
  const data = useStaticQuery(graphql`
    query {
      ayasanapp: file(absolutePath: { regex: "/ayasanapp/" }) {
        childImageSharp {
          fixed(width: 300) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      invesdiarylogo: file(absolutePath: { regex: "/invesdiarylogo/" }) {
        childImageSharp {
          fixed(width: 80) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      bloomon: file(absolutePath: { regex: "/bloomon/" }) {
        childImageSharp {
          fixed(width: 300) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `)

  const { invesdiarylogo, bloomon, ayasanapp } = data

  return (
    <Container ref={forwardedRef} backgroundColor="#142f46">
      <HeaderYear year="2016"/>
      <Section
        reverse
        title="Launch Ayasan-App.com"
        description={<>After Ayasan Application are succesfully launched, he then quickly luanch its <a href="https://ayasan-app.com">ayasan-app.com</a> website. He use his React knowledge learned from Kaidee to craft this product.</>}
        showcase={
          <Image
            fixed={ayasanapp.childImageSharp.fixed}
            alt="ayasan app"
            style={{
              width: 300
            }}
          />
        }
      />
      <Section
        title="Investdiary Founded"
        description={<>He founded <a href="https://facebook.com/investdiary">Investdiary</a> Facebook Page during October of 2016. The idea is to write down what he learn about investment.</>}
        showcase={
          <Image
            fixed={invesdiarylogo.childImageSharp.fixed}
            alt="invesdiary"
            style={{
              width: 80
            }}
          />
        }
      />
      <Section
        reverse
        title="Work Remote with Bloomon"
        description={<>In late November, he left Kaidee and start a new journey with <a href="https://www.bloomon.co.uk">Bloomon</a>. He works remotely with people from Taipei and Amsterdam. His main role is to build a website using Angular, React/Redux.</>}
        showcase={
          <Image
            fixed={bloomon.childImageSharp.fixed}
            alt="bloomon"
          />
        }
      />
    </Container>
  )
})

export default forwardRef((props, ref) => <Year {...props} forwardedRef={ref} />)
