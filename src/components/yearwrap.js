import React, { forwardRef } from 'react'
import { Row, Col } from 'antd'
import styled from 'styled-components'
import ScrollAnimation from 'react-animate-on-scroll'

export const Header = ({ top, bottom }) => {
  return (
    <Row>
      <header>
        <h2>{top}<span>{bottom}</span></h2>
      </header>
    </Row>
  )
}

export const HeaderYear = ({ year }) => <Header top="Journey in the" bottom={year}/>

export const Section = ({ title = '', description, reverse = false, showcase }) => {
  const animation = ['fadeInUp', 'fadeInUpBig', 'zoomIn', 'rollIn', 'bounceIn', 'flipInY', 'flipInX']
  return (
    <Row type="flex" justify="center" align="middle" className={`inner ${reverse ? 'reverse' : ''}`} gutter={32}>
      <Col xs={24} md={12}>
        <Row type="flex" justify="center" align="middle">
          <ScrollAnimation animateIn={animation[Math.floor(Math.random() * animation.length)]} animateOut="fadeOut" offset={50}>
            {showcase}
          </ScrollAnimation>
        </Row>
      </Col>
      <Col xs={24} md={12}>
        <h3>{title}</h3>
        <p>{description}</p>
      </Col>
    </Row>
  )
}

const StyledDiv = styled.div`
  position: relative;
  background-color: ${props => props.backgroundColor};
  min-height: ${props => props.noLine ? '50' : '100'}vh;
  width: 100vw;
  text-align: center;
  padding: 25px 3%;

  :after {
    @media (min-width: 768px) {
      content: ' ';
      position: absolute;
      top: 0;
      left: 50%;
      border-left: 1px solid #FFF;
      height: ${props => props.noLine ? '25px' : '100%'};
    }
  }

  header {
    position: relative;
    background-color: ${props => props.backgroundColor};
    z-index: 1;
  }

  h2 {
    font-size: 14px;
    margin: 0;
    padding: 20px 0 10px;
    color: white;
  }

  h2 span {
    position: relative;
    display: block;
    overflow: visible;
    top: 50%;
    font-size: 40px;
  }

  .inner {
    position: relative;
    top: 20px;
    z-index: 1;
    margin-bottom: 40px;
  }

  h3, p {
    color: white;
    text-align: start;
  }

  .reverse {
    flex-direction: row-reverse;

    p, h3 {
      text-align: end;
    }
  }

  @media only screen and (max-width: 767px) {
    h3 {
      margin-top: 10px;
    }
    p, h3 {
      text-align: center !important;
    }
  }

  header:before, header:after {
    content: ' ';
    display: block;
    left: 50%;
    top: 0%;
    margin: auto;
    width: 20px;
    height: 20px;
    border: 5px solid #FFF;
    border-radius: 10px;
    z-index: 10;
  }
`;

export const Container = forwardRef(({ children, ...rest }, ref) => {
  return (
    <StyledDiv ref={ref} {...rest}>
      <Row type="flex" justify="center">
        <Col xs={24} sm={20} md={22}>
          {children}
        </Col>
      </Row>
    </StyledDiv>
  )
})

