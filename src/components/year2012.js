import React, { memo, forwardRef } from 'react'
import { Col } from 'antd'
import { useStaticQuery, graphql } from 'gatsby'
import { Container, Section, HeaderYear } from './yearwrap'
import Image from 'gatsby-image'

const Year = memo(({ forwardedRef }) => {
  const data = useStaticQuery(graphql`
    query {
      siit: file(absolutePath: { regex: "/siit/" }) {
        childImageSharp {
          fixed(width: 300) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      wafflehouse: file(absolutePath: { regex: "/wafflehouse/" }) {
        childImageSharp {
          fixed(width: 150) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      wendys: file(absolutePath: { regex: "/wendys/" }) {
        childImageSharp {
          fixed(width: 150) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `)

  const { siit, wafflehouse, wendys } = data

  return (
    <Container ref={forwardedRef} backgroundColor="#3883d8">
      <HeaderYear year="2012" />
      <Section
        title="Graduated from SIIT"
        description="In Early 2012, he was graduated at SIIT, Thammasat University major in Computer Science."
        showcase={
          <Image
            fixed={siit.childImageSharp.fixed}
            alt="siit"
          />
        }
      />
      <Section
        reverse
        title="Work in USA"
        description="Right after graduation, he went to Destin, Florida and works as a waiter at Waffle House and Wendy's."
        showcase={
          <>
            <Col>
              <Image
                fixed={wafflehouse.childImageSharp.fixed}
                alt="wafflehouse"
              />
            </Col>
            <Col>
              <Image
                fixed={wendys.childImageSharp.fixed}
                alt="wendys"
              />
            </Col>
          </>
        }
      />
    </Container>
  )
})

export default forwardRef((props, ref) => <Year {...props} forwardedRef={ref} />)
