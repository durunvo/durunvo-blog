import React, { memo } from 'react'
import { Link } from 'gatsby'
import { rhythm } from '../utils/typography'
import { List } from 'antd'
import Image from 'gatsby-image'

export default memo(({ posts = [], currentPage, totatPosts }) => {
  return (
    <div
      style={{
        marginLeft: `auto`,
        marginRight: `auto`,
        maxWidth: rhythm(24),
        padding: `${rhythm(2)} ${rhythm(3 / 4)}`,
      }}
    >
      <h2>Explore Blog</h2>
      <List
        itemLayout="vertical"
        size="large"
        pagination={{
          onChange: page => {
            console.log(page);
          },
          pageSize: 6,
          current: currentPage,
          total: totatPosts,
          showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} posts`
        }}
        dataSource={posts}
        renderItem={(item, index) => (
          <List.Item
            key={index}
            actions={[
              <Link to={item.node.fields.slug}>Read more</Link>,
            ]}
            extra={
              <Image
                alt={item.node.frontmatter.title}
                fixed={item.node.frontmatter.image.childImageSharp.fixed}
              />
            }
          >
            <List.Item.Meta
              title={<Link to={item.node.fields.slug}>{item.node.frontmatter.title}</Link>}
            />
            {item.node.frontmatter.description}
          </List.Item>
        )}
      />
    </div>
  )
})
