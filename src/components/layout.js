import React, { PureComponent } from 'react'
import { Link } from 'gatsby'
import { rhythm, scale } from '../utils/typography'

class Layout extends PureComponent {
  render() {
    const { location, title, description, children } = this.props
    const rootPath = `${__PATH_PREFIX__}/`
    let header

    if (location.pathname === rootPath) {
      header = (
        <>
          <h1
            style={{
              ...scale(1.4),
              marginBottom: rhythm(1.5),
              marginTop: 0,
            }}
          >
            {title}
          </h1>
          <p style={{ position: 'relative', top: -45 }}>{description}</p>
        </>
      )
    } else {
      header = (
        <>
          <h3
            style={{
              fontFamily: `Montserrat, sans-serif`,
              marginTop: 0,
            }}
          >
            {title}
          </h3>
          <p style={{ position: 'relative', top: -12 }}>{description}</p>
        </>
      )
    }
    return (
      <div>
        <header
          style={{
            marginLeft: `auto`,
            marginRight: `auto`,
            maxWidth: rhythm(24),
            padding: `${rhythm(1.5)} ${rhythm(3 / 4)}`,
          }}>
          <Link
            style={{
              boxShadow: `none`,
              textDecoration: `none`,
              color: `inherit`,
            }}
            to="/"
          >
          {header}
          </Link>
        </header>
        <main style={{ position: 'relative' }}>{children}</main>
        <footer
          style={{
            marginTop: rhythm(1.5),
            borderTop: '1px solid lightgray',
            paddingTop: rhythm(1.5),
          }}
        >
          <small>© {new Date().getFullYear()}, Built with <a href="https://www.gatsbyjs.org">gatsby</a> and <a href="https://ant.design">ant.design</a></small>
        </footer>
      </div>
    )
  }
}

export default Layout
