// @flow
import React, { memo } from 'react'
// import { Controller, Scene } from 'react-scrollmagic'
import Bio from '../components/bio'
import Layout from '../components/layout'
import SEO from '../components/seo'
import Year2012 from '../components/year2012'
import Year2015 from '../components/year2015'
import Year2016 from '../components/year2016'
import Year2017 from '../components/year2017'
import Year2018 from '../components/year2018'
import Year2019 from '../components/year2019'
import ThankYou from '../components/thankyou'
import Blog from '../components/exploreblog'

const Index = ({ data, location }) => {

  const siteTitle = data.site.siteMetadata.title
  const siteDescription = data.site.siteMetadata.description
  const { edges: posts, totalCount } = data.allMarkdownRemark

  return (
    <Layout location={location} title={siteTitle} description={siteDescription}>
      <SEO title="A Life Journey" />
      <Bio />
      <Year2012 />
      <Year2015 />
      <Year2016 />
      <Year2017 />
      <Year2018 />
      <Year2019 />
      <ThankYou />
      <Blog posts={posts} currentPage={1} totatPosts={totalCount}/>
    </Layout>
  )
}

export default memo(Index)

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
        description
      }
    }
    allMarkdownRemark(
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { fileAbsolutePath: { regex: "/blog/" } }
      limit: 6
      skip: 0
    ) {
      totalCount
      edges {
        node {
          excerpt
          fields {
            slug
          }
          frontmatter {
            date(formatString: "MMMM DD, YYYY")
            title
            description
            image {
              childImageSharp {
                fixed(width: 300) {
                  ...GatsbyImageSharpFixed
                }
              }
            }
          }
        }
      }
    }
  }
`
