import React, { PureComponent } from 'react'
import { Link, graphql } from 'gatsby'
import Image from 'gatsby-image'

import Bio from '../components/bio'
import Layout from '../components/layout'
import SEO from '../components/seo'
import { rhythm } from '../utils/typography'

/**
 * TODO: http://www.akita.co.uk/computing-history/
 * TODO: https://www.gramercyparkhotel.com/hotel/history
 */

class BlogIndex extends PureComponent {
  render() {
    const { data } = this.props
    
    const siteTitle = data.site.siteMetadata.title
    const siteDescription = data.site.siteMetadata.description
    const posts = data.allMarkdownRemark.edges

    const WebsiteImage = ({ fixed, alt }) => (
      <Image
        fixed={fixed}
        alt={alt}
        style={{
          maxWidth: '100%'
        }}
      />
    )

    return (
      <Layout location={this.props.location} title={siteTitle} description={siteDescription}>
        <SEO title="Home" />
        <Bio />
        <h1>Websites</h1>
        <h3>Investdiary - <a href="https://investdiary.co"><i><small>investdiary.co</small></i></a></h3>
        <WebsiteImage
          fixed={data.investdiary.childImageSharp.fixed}
          alt="investdiary.co"
        />
        <h3>Ayasan App - <a href="https://www.ayasan-app.com"><i><small>www.ayasan-app.com</small></i></a></h3>
        <WebsiteImage
          fixed={data.ayasan.childImageSharp.fixed}
          alt="www.ayasan-app.com"
        />
        <h3>PropaChill - <a href="https://www.propachill.com"><i><small>www.propachill.com</small></i></a></h3>
        <WebsiteImage
          fixed={data.propachill.childImageSharp.fixed}
          alt="www.propachill.com"
        />
        <h3>PropaChill Blog - <a href="https://blog.propachill.com"><i><small>blog.propachill.com</small></i></a></h3>
        <WebsiteImage
          fixed={data.blogpropachill.childImageSharp.fixed}
          alt="blog.propachill.com"
        />
        {/* <h2>Articles</h2> */}
        {
          // posts.map(({ node }) => {
          //   const title = node.frontmatter.title || node.fields.slug
          //   return (
          //     <div key={node.fields.slug}>
          //       <h3
          //         style={{
          //           marginBottom: rhythm(1 / 4),
          //         }}
          //       >
          //         <Link style={{ boxShadow: `none` }} to={node.fields.slug}>
          //           {title}
          //         </Link>
          //       </h3>
          //       <small>{node.frontmatter.date}</small>
          //       <p
          //         dangerouslySetInnerHTML={{
          //           __html: node.frontmatter.description || node.excerpt,
          //         }}
          //       />
          //     </div>
          //   )
          // })
        }
      </Layout>
    )
  }
}

export default BlogIndex

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
        description
      }
    }
    ayasan: file(absolutePath: { regex: "/ayasanapp.png/" }) {
      childImageSharp {
        fixed(width: 600) {
          ...GatsbyImageSharpFixed
        }
      }
    }
    investdiary: file(absolutePath: { regex: "/investdiary.png/" }) {
      childImageSharp {
        fixed(width: 600) {
          ...GatsbyImageSharpFixed
        }
      }
    }
    propachill: file(absolutePath: { regex: "/\/propachill.png/" }) {
      childImageSharp {
        fixed(width: 600) {
          ...GatsbyImageSharpFixed
        }
      }
    }
    blogpropachill: file(absolutePath: { regex: "/blogpropachill.png/" }) {
      childImageSharp {
        fixed(width: 600) {
          ...GatsbyImageSharpFixed
        }
      }
    }
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      edges {
        node {
          excerpt
          fields {
            slug
          }
          frontmatter {
            date(formatString: "MMMM DD, YYYY")
            title
            description
          }
        }
      }
    }
  }
`
